#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Servo.h>

Servo doorServo;
// ใช้ address ที่สแกนพบ 0x20
LiquidCrystal_I2C lcd(0x20, 16, 2);

const int buttonOpen = 2;
const int buttonClose = 3;
const int buttonFloor1 = 4;
const int buttonFloor2 = 5;
const int buttonFloor3 = 6;
const int motorPin1 = 10;
const int motorPin2 = 11;

int currentFloor = 1;
bool moving = false;

void setup() {
  pinMode(buttonOpen, INPUT);
  pinMode(buttonClose, INPUT);
  pinMode(buttonFloor1, INPUT);
  pinMode(buttonFloor2, INPUT);
  pinMode(buttonFloor3, INPUT);
  pinMode(motorPin1, OUTPUT);
  pinMode(motorPin2, OUTPUT);

  doorServo.attach(9);

  lcd.init();
  lcd.backlight();
  lcd.clear();

  Serial.begin(9600);
}

void loop() {
  // ตรวจสอบสถานะของปุ่มเปิดประตู
  if (digitalRead(buttonOpen) == HIGH && !moving) {
    doorServo.write(90); // เปิดประตู
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Door Status:");
    lcd.setCursor(0, 1);
    lcd.print("Opening");
    delay(1000); // เพิ่ม delay ให้การแสดงผลบนหน้าจอ LCD ชัดเจนขึ้น
  }

  // ตรวจสอบสถานะของปุ่มปิดประตู
  if (digitalRead(buttonClose) == HIGH && !moving) {
    doorServo.write(0); // ปิดประตู
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Door Status:");
    lcd.setCursor(0, 1);
    lcd.print("Closing");
    delay(1000); // เพิ่ม delay ให้การแสดงผลบนหน้าจอ LCD ชัดเจนขึ้น
  }

  // เคลื่อนลิฟท์ไปชั้น 1
  if (digitalRead(buttonFloor1) == HIGH && !moving) {
    moveToFloor(1);
  }
  
  // เคลื่อนลิฟท์ไปชั้น 2
  if (digitalRead(buttonFloor2) == HIGH && !moving) {
    moveToFloor(2);
  }
  
  // เคลื่อนลิฟท์ไปชั้น 3
  if (digitalRead(buttonFloor3) == HIGH && !moving) {
    moveToFloor(3);
  }
}

void moveToFloor(int targetFloor) {
  moving = true;
  lcd.clear();
  if (currentFloor > targetFloor) {
    while (currentFloor > targetFloor) {
      currentFloor--;
      digitalWrite(motorPin1, HIGH);
      digitalWrite(motorPin2, LOW);
      lcd.setCursor(0, 0);
      lcd.print("On the way to");
      lcd.setCursor(0, 1);
      lcd.print("floor ");
      lcd.print(currentFloor);
      delay(1000);
      lcd.clear();
    }
  } else if (currentFloor < targetFloor) {
    while (currentFloor < targetFloor) {
      currentFloor++;
      digitalWrite(motorPin1, LOW);
      digitalWrite(motorPin2, HIGH);
      lcd.setCursor(0, 0);
      lcd.print("On the way to");
      lcd.setCursor(0, 1);
      lcd.print("floor ");
      lcd.print(currentFloor);
      delay(1000);
      lcd.clear();
    }
  }

  digitalWrite(motorPin1, LOW);
  digitalWrite(motorPin2, LOW);
  lcd.setCursor(0, 0);
  lcd.print("Arrived at");
  lcd.setCursor(0, 1);
  lcd.print("floor ");
  lcd.print(currentFloor);
  delay(2000);
  lcd.clear();
  moving = false;
}
